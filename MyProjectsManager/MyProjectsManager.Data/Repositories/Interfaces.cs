﻿// Author: Emir Mamashov
// Project: MyProjectsManager.Data 
// DateCreate: 12.04.2016  23:34
// Location: Kyrgyzstan. Bishkek - 2016

using MyProjectsManager.Data.Repositories.GenericRepository;

namespace MyProjectsManager.Data.Repositories
{
    #region IAspNetUsersRepository
    public interface IAspNetUsersRepository : IGenericRepository<AspNetUser>
    {
    }
    #endregion  
    #region IPositionsRepository
    public interface IPositionsRepository : IGenericRepository<Position>
    {
    }
    #endregion  

    #region IProjectsRepository
    public interface IProjectsRepository : IGenericRepository<Project>
    {
    }
    #endregion  

    #region ITasksRepository
    public interface ITasksRepository : IGenericRepository<Task>
    {
    }
    #endregion  

    #region ITeamsRepository
    public interface ITeamsRepository : IGenericRepository<Team>
    {
    }
    #endregion  

    #region IUserInProjectsRepository
    public interface IUserInProjectsRepository : IGenericRepository<UserInProject>
    {
    }
    #endregion  

    #region IUserInTeamsRepository
    public interface IUserInTeamsRepository : IGenericRepository<UserInTeam>
    {
    }
    #endregion  

    #region IStatusRepository
    public interface IStatusRepository : IGenericRepository<Status>
    {
    }
    #endregion  

    #region ICommentsForTaskRepository
    public interface ICommentsForTaskRepository : IGenericRepository<CommentsForTask>
    {
    }
    #endregion  
}