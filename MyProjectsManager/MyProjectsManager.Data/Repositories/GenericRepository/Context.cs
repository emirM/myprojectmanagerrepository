﻿// Author: Emir Mamashov
// Project: MyProjectsManager.Data 
// DateCreate: 12.04.2016  23:32
// Location: Kyrgyzstan. Bishkek - 2016

using System.Configuration;
using System.Data.Entity;

namespace MyProjectsManager.Data.Repositories.GenericRepository
{
    public class Context<T> : IContext<T> where T : class
    {
        public DbContext DbContext { get; private set; }
        public IDbSet<T> DbSet { get; private set; }

        public Context()
        {
            DbContext = new DbContext(ConfigurationManager.ConnectionStrings["MyProjectsManagerEntities"].ConnectionString);
            DbSet = DbContext.Set<T>();
        }

        public void Dispose()
        {
            DbContext.Dispose();
        }

    }
}