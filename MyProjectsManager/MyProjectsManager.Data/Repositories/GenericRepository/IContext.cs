﻿// Author: Emir Mamashov
// Project: MyProjectsManager.Data 
// DateCreate: 12.04.2016  23:31
// Location: Kyrgyzstan. Bishkek - 2016

using System;
using System.Data.Entity;

namespace MyProjectsManager.Data.Repositories.GenericRepository
{
    public interface IContext<T> : IDisposable where T : class
    {
        DbContext DbContext { get; }
        IDbSet<T> DbSet { get; }
    }
}