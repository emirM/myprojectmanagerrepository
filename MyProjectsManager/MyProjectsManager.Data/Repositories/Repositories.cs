﻿// Author: Emir Mamashov
// Project: MyProjectsManager.Data 
// DateCreate: 12.04.2016  23:40
// Location: Kyrgyzstan. Bishkek - 2016

using MyProjectsManager.Data.Repositories.GenericRepository;

namespace MyProjectsManager.Data.Repositories
{
    #region AspNetUsersRepository
    public class AspNetUsersRepository : GenericRepository<AspNetUser>, IAspNetUsersRepository
    {

    }
    #endregion
    #region PositionsRepository
    public class PositionsRepository : GenericRepository<Position>, IPositionsRepository
    {

    }
    #endregion

    #region ProjectsRepository
    public class ProjectsRepository : GenericRepository<Project>, IProjectsRepository
    {

    }
    #endregion

    #region TasksRepository
    public class TasksRepository : GenericRepository<Task>, ITasksRepository
    {

    }
    #endregion

    #region TeamsRepository
    public class TeamsRepository : GenericRepository<Team>, ITeamsRepository
    {

    }
    #endregion

    #region UserInProjectsRepository
    public class UserInProjectsRepository : GenericRepository<UserInProject>, IUserInProjectsRepository
    {

    }
    #endregion

    #region UserInTeamsRepository
    public class UserInTeamsRepository : GenericRepository<UserInTeam>, IUserInTeamsRepository
    {

    }
    #endregion

    #region StatusRepository
    public class StatusRepository : GenericRepository<Status>, IStatusRepository
    {

    }
    #endregion

    #region CommentsForTasksRepository

    public class CommentsForTasksRepository : GenericRepository<CommentsForTask>, ICommentsForTaskRepository
    {
        
    }
    #endregion
}