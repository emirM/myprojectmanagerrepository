﻿// Author: Emir Mamashov
// Project: MyProjectsManager.Data 
// DateCreate: 06.05.2016  14:04
// Location: Kyrgyzstan. Bishkek - 2016

using System;
using System.ComponentModel.DataAnnotations;

namespace MyProjectsManager.Data
{
    [MetadataType(typeof(StatusMetaData))]
    public partial class Status
    {
         
    }

    public class StatusMetaData
    {
        [Display(Name = "Нименования")]
        [Required(ErrorMessage = "Поле обязательно для заполнения")]
        public string Name { get; set; }
        
        [Display(Name = "Дата создания")]
        [Required]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:dd'/'MM'/'yyyy' 'hh':'mm':'ss}", ApplyFormatInEditMode = true)]
        public DateTime DateCreate { get; set; }

        [Display(Name = "Дата изменения")]
        [Required]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:dd'/'MM'/'yyyy' 'hh':'mm':'ss}", ApplyFormatInEditMode = true)]
        public DateTime? DateModified { get; set; }
    }
}