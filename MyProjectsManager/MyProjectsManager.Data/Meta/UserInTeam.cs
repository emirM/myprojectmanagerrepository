﻿// Author: Emir Mamashov
// Project: MyProjectsManager.Data 
// DateCreate: 12.04.2016  23:29
// Location: Kyrgyzstan. Bishkek - 2016

using System.ComponentModel.DataAnnotations;

namespace MyProjectsManager.Data
{
    [MetadataType(typeof(UserInTeamMetaData))]
    public partial class UserInTeam
    {
         
    }

    public class UserInTeamMetaData
    {
        [Required]
        public string UserId { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:dd'/'MM'/'yyyy' 'hh':'mm':'ss}", ApplyFormatInEditMode = true)]
        public string TeamId { get; set; }
    }
}