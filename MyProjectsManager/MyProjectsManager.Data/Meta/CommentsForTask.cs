﻿// Project: MyProjectsManager.Data
// Craete date: 02.07.2016 3:34 PM
// Author: Emir Mamashov
// Email: emir.mamashov@gmail.com

using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MyProjectsManager.Data
{
    [MetadataType(typeof(CommentsForTaskMetaData))]
    public partial class CommentsForTask
    {
         
    }

    public class CommentsForTaskMetaData
    {
        [Required]
        [AllowHtml]
        [DataType(DataType.MultilineText)]
        public string Text { get; set; }
        public string TaskId { get; set; }
        public string UserId { get; set; }

        [Display(Name = "Attach file")]
        public string FileUrl { get; set; }

        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:dd'.'MM'.'yyyy' 'hh':'mm':'ss}", ApplyFormatInEditMode = true)]
        public DateTime? DateCreate { get; set; }
    }
}