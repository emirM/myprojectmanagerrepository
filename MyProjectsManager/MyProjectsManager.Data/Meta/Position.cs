﻿// Author: Emir Mamashov
// Project: MyProjectsManager.Data 
// DateCreate: 12.04.2016  23:16
// Location: Kyrgyzstan. Bishkek - 2016

using System;
using System.ComponentModel.DataAnnotations;

namespace MyProjectsManager.Data
{
    [MetadataType(typeof(PositionMetaData))]
    public partial class Position
    {
         
    }

    public class PositionMetaData
    {
        [Required]
        public string Title { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:dd'/'MM'/'yyyy' 'hh':'mm':'ss}", ApplyFormatInEditMode = true)]
        public DateTime DateCreate { get; set; }

        public DateTime? DateModified { get; set; }
    }
}