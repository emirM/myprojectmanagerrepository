﻿// Author: Emir Mamashov
// Project: MyProjectsManager.Data 
// DateCreate: 13.04.2016  22:53
// Location: Kyrgyzstan. Bishkek - 2016

using System;
using System.ComponentModel.DataAnnotations;

namespace MyProjectsManager.Data
{
    [MetadataType(typeof(AspNetUserMetaData))]
    public partial class AspNetUser
    {

    }
    public class AspNetUserMetaData
    {
        [Required]
        public string Email { get; set; }
        
        public bool EmailConfirmed { get; set; }
        public string PasswordHash { get; set; }
        public string SecurityStamp { get; set; }
        public string PhoneNumber { get; set; }
        public bool PhoneNumberConfirmed { get; set; }
        public bool TwoFactorEnabled { get; set; }
        public DateTime? LockoutEndDateUtc { get; set; }
        public bool LockoutEnabled { get; set; }
        public int AccessFailedCount { get; set; }

        [Required]
        public string UserName { get; set; }
        public string PositionId { get; set; }

    }

}