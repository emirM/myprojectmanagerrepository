﻿// Author: Emir Mamashov
// Project: MyProjectsManager.Data 
// DateCreate: 25.05.2016  14:25
// Location: Kyrgyzstan. Bishkek - 2016
namespace MyProjectsManager.Data
{
    public class Error
    {
        public int Code { get; set; }
        public string Description { get; set; }
        public string Result { get; set; }
    }
}