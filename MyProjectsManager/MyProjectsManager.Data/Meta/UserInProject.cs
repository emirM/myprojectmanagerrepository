﻿// Author: Emir Mamashov
// Project: MyProjectsManager.Data 
// DateCreate: 12.04.2016  23:26
// Location: Kyrgyzstan. Bishkek - 2016

using System;
using System.ComponentModel.DataAnnotations;

namespace MyProjectsManager.Data
{
    [MetadataType(typeof(UserInProjectMetaData))]
    public partial class UserInProject
    {
         
    }

    public class UserInProjectMetaData
    {
        [Required]
        public string UserId { get; set; }

        [Required]
        public string ProjectId { get; set; }

        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:dd'/'MM'/'yyyy' 'hh':'mm':'ss}", ApplyFormatInEditMode = true)]
        public DateTime? DateCreate { get; set; }
    }
}