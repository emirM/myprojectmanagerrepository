﻿// Author: Emir Mamashov
// Project: MyProjectsManager.Data 
// DateCreate: 12.04.2016  22:57
// Location: Kyrgyzstan. Bishkek - 2016

using System;
using System.Configuration;
using System.Web.Configuration;

namespace MyProjectsManager.Data.Helpers
{
    internal class SettingsHelper
    {
        private static String emailSender = "";
        private static String emailPassword = "";
        private static Int32 emailSMTPPort = -1;
        private static String emailSMTP = "";

        private static String publicKey;
        private static String privateKey;

        public static String PublicKey
        {
            get
            {
                var subject = WebConfigurationManager.AppSettings["PublicKey"];

                if (String.IsNullOrEmpty(subject))
                    throw new ConfigurationErrorsException(
                        "Web Configuration file must contain <appSettings><add key=\"PublicKey\".../></appSettings>");
                publicKey = subject.Replace(".", "<").Replace(",", ">");
                return publicKey;
            }

        }
        public static String PrivateKey
        {
            get
            {
                var subject = WebConfigurationManager.AppSettings["PrivateKey"];

                if (String.IsNullOrEmpty(subject))
                    throw new ConfigurationErrorsException(
                        "Web Configuration file must contain <appSettings><add key=\"PrivateKey\".../></appSettings>");
                privateKey = subject.Replace(".", "<").Replace(",", ">");
                return privateKey;
            }

        }

        public static String EmailSender
        {
            get
            {
                if (emailSender == "")
                {
                    var subject = WebConfigurationManager.AppSettings["EmailSender"];

                    if (String.IsNullOrEmpty(subject))
                        throw new ConfigurationErrorsException(
                            "Web Configuration file must contain <appSettings><add key=\"EmailSender\".../></appSettings>");
                    emailSender = subject;
                }

                return emailSender;
            }
        }

        public static String EmailPassword
        {
            get
            {
                if (emailPassword == "")
                {
                    var subject = WebConfigurationManager.AppSettings["EmailPassword"];

                    if (String.IsNullOrEmpty(subject))
                        throw new ConfigurationErrorsException(
                            "Web Configuration file must contain <appSettings><add key=\"EmailPassword\".../></appSettings>");
                    emailPassword = subject;
                }

                return emailPassword;
            }
        }

        public static String EmailSMTP
        {
            get
            {
                if (emailSMTP == "")
                {
                    var subject = WebConfigurationManager.AppSettings["EmailSMTP"];

                    if (String.IsNullOrEmpty(subject))
                        throw new ConfigurationErrorsException(
                            "Web Configuration file must contain <appSettings><add key=\"EmailSMTP\".../></appSettings>");
                    emailSMTP = subject;
                }

                return emailSMTP;
            }
        }

        public static Int32 EmailSMTPPort
        {
            get
            {
                if (emailSMTPPort == -1)
                {
                    var appSettingPortValue = WebConfigurationManager.AppSettings["EmailSMTPPort"];

                    if (String.IsNullOrEmpty(appSettingPortValue))
                        throw new ConfigurationErrorsException(
                            "Web Configuration file must contain <appSettings><add key=\"EmailSMTPPort\".../></appSettings>");

                    Int32 portValue;
                    if (!Int32.TryParse(appSettingPortValue, out portValue))
                        throw new ConfigurationErrorsException("key - \"EmailSMTPPort\" must contain integer value");

                    emailSMTPPort = portValue;
                }

                return emailSMTPPort;
            }
        }


    }

}