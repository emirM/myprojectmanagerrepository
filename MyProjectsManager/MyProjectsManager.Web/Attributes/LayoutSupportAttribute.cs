﻿// Author: Emir Mamashov
// Project: MyProjectsManager.Web 
// DateCreate: 13.04.2016  23:09
// Location: Kyrgyzstan. Bishkek - 2016

using System.Linq;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using MyProjectsManager.Web.Controllers;

namespace MyProjectsManager.Web.Attributes
{
    [Authorize]
    public class LayoutSupportAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);
            var controller = (BaseController)filterContext.Controller;

            var aspNetUserId = controller.User.Identity.GetUserId();
            if (aspNetUserId == null) return;

            var currentUser = controller.AspNetUsersRepository.GetBy(x=>x.Id==aspNetUserId).FirstOrDefault();
           
            if (controller.TeamsRepository == null || currentUser == null) return;
            controller.ViewBag.CurrentUser = currentUser;
            controller.CurrentUser = currentUser;
            var teams = controller.TeamsRepository.GetBy(x=>x.OwnUserId==aspNetUserId || x.UserInTeams.FirstOrDefault(y=>y.UserId==aspNetUserId).UserId==aspNetUserId);

            controller.ViewBag.Teams = teams;

        }
    }
}