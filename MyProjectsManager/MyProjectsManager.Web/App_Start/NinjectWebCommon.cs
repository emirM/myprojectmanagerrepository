using MyProjectsManager.Data.Repositories;

[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(MyProjectsManager.Web.App_Start.NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(MyProjectsManager.Web.App_Start.NinjectWebCommon), "Stop")]

namespace MyProjectsManager.Web.App_Start
{
    using System;
    using System.Web;

    using Microsoft.Web.Infrastructure.DynamicModuleHelper;

    using Ninject;
    using Ninject.Web.Common;

    public static class NinjectWebCommon 
    {
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start() 
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }
        
        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            bootstrapper.ShutDown();
        }
        
        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            try
            {
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

                RegisterServices(kernel);
                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
            kernel.Bind(typeof(IAspNetUsersRepository)).To(typeof(AspNetUsersRepository));
            kernel.Bind(typeof(IPositionsRepository)).To(typeof(PositionsRepository));
            kernel.Bind(typeof(IProjectsRepository)).To(typeof(ProjectsRepository));
            kernel.Bind(typeof(ITasksRepository)).To(typeof(TasksRepository));
            kernel.Bind(typeof(ITeamsRepository)).To(typeof(TeamsRepository));
            kernel.Bind(typeof(IUserInProjectsRepository)).To(typeof(UserInProjectsRepository));
            kernel.Bind(typeof(IUserInTeamsRepository)).To(typeof(UserInTeamsRepository));
            kernel.Bind(typeof (IStatusRepository)).To(typeof (StatusRepository));
            kernel.Bind(typeof (ICommentsForTaskRepository)).To(typeof (CommentsForTasksRepository));
        }
    }
}
