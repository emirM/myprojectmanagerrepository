﻿
var timerTask = {
    "id": "",
    "isStarting": false
};
var idIntervals = 0;

function startTimer() {
    var result = {
        "code": 0,
        "description": "ok"
    };
    if (!timerTask.isStarting) {
        result.code = 1;
        result.description = "timer off";
        return result;
    }
    if (!timerTask.id) {
        result.code = 2;
        result.description = "id is null";
        return result;
    } 
    //if (idTask!=timerTask.id) {
    //    result.code = 3;
    //    result.description = "id not found";
    //    return result;
    //} 
    //var my_timer = document.getElementById("my_timer");
    var time = $("#" + timerTask.id + " #timer")[0].innerText;
    var arr = time.split(":");
    var h = arr[0];
    var m = arr[1];
    var s = arr[2];
    if (parseInt(s) >= 59) {
        if (parseInt(m) < 9) {
            m = "0" + (parseInt(m) + 1);
        } else {
            m++;
        }
        s = "00";
    } else if (parseInt(s) < 9) {
        s = "0" + (parseInt(s) + 1);
    } else {
        s++;
    }

    if (parseInt(m) >= 59) {
        if (parseInt(h) < 9) {
            h = "0" + (parseInt(h) + 1);
        } else {
            h++;
        }
        m = "00";
    }

    $("#" + timerTask.id + " #timer")[0].innerHTML = h + ":" + m + ":" + s;
    //setTimeout(startTimer, 1000);
    return result;
}

function stopOthersTimer() {
    var timers = $(".timer");
    for (var i = 0; i < timers.length; i++) {
        if (timers[i].style.display != "none") {
            timers[i].style.display = "none";
            setSpendTime(timers[i].parentElement.id, timers[i].innerText);
        }

        //setSpendTime(idTask, taskTimer[0].innerText);
    }
}

function setSpendTime(idTask,time) {
    $.ajax({
        url: '/Tasks/SpentTimeSet',
        type: 'POST',
        data: {
            id: idTask,
            time: time
        },
        success: function (result) {
            if (result.Code == 0) {
                //alert(result.Description);
                //var res = JSON.parse(result.Result);
                //viewResult(res);
            }
        },
        error: function (result) {
            //    var str = result.responseText.split('{');
            //    var resultStr = JSON.parse(result.responseText.substr(str[0].length));
            //if (resultStr.Code == 0) {
            //    viewResult(JSON.parse(resultStr.Result));
            //}
        }
    });
}

function setComplete(idTask, isComplete) {
    var complete = $("#" + idTask + " #Complete");
    var uncomplete = $("#" + idTask + " #Uncomplete");
    var completeImg=$("#" + idTask + " #CompleteImg");
    var noactiveImg=$("#" + idTask + " #NoActiveImg");
    if (isComplete) {
        complete[0].style.display = "none";
        uncomplete[0].style.display = "";

        completeImg[0].style.display = "";
        noactiveImg[0].style.display = "none";
        clearInterval(idIntervals);
        var taskTimer = $("#" + idTask + " #timer");
        taskTimer[0].style.display = "none";
    } else {
        complete[0].style.display = "";
        uncomplete[0].style.display = "none";

        completeImg[0].style.display = "none";
        noactiveImg[0].style.display = "";
    }
    $.ajax({
        url: '/Tasks/Complete',
        type: 'POST',
        data: {
            id: idTask,
            isComplete: isComplete
        },
        success: function (result) {
            if (result.Code == 0) {
                //alert(result.Description);
                //var res = JSON.parse(result.Result);
                //viewResult(res);
            } 
            //else {
                //    alert(result.Description);
                //}
            },
        error: function (result) {
            //    var str = result.responseText.split('{');
            //    var resultStr = JSON.parse(result.responseText.substr(str[0].length));
            //if (resultStr.Code == 0) {
            //    viewResult(JSON.parse(resultStr.Result));
            //}
        }
    });
}

function startOrStopTask(idTask) {
    //clearTimeout(idIntervals);
    clearInterval(idIntervals);
    var taskTimer = $("#" + idTask + " #timer");
    if (taskTimer) {
        if (taskTimer[0].style.display == "none") {
            stopOthersTimer();
            timerTask.id = idTask;
            timerTask.isStarting = true;
            taskTimer[0].style.display = "";
            idIntervals = setInterval(function () {
                 startTimer();
            }, 1000);
        } else {
            stopOthersTimer();
            timerTask.id = idTask;
            timerTask.isStarting = false;
            taskTimer[0].style.display = "none";
        }
    }
}

window.onbeforeunload = function () {//на закрытие и на обнавление вкладки
    //alert("Что-нибудь сообщить пользователю");
    stopOthersTimer();
}