﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MyProjectsManager.Data;
using MyProjectsManager.Data.Repositories;
using MyProjectsManager.Web.Attributes;

namespace MyProjectsManager.Web.Controllers
{
    [LayoutSupport]
    [Authorize]
    public class CommentsForTasksController : BaseController
    {
        public override IAspNetUsersRepository AspNetUsersRepository { get; set; }
        public override ITeamsRepository TeamsRepository { get; set; }
        private ICommentsForTaskRepository _commentsForTaskRepository { get; set; }
        private ITasksRepository _tasksRepository { get; set; }

        public CommentsForTasksController(ITasksRepository tasksRepository, IAspNetUsersRepository aspNetUsersRepository,
            ITeamsRepository teamsRepository, ICommentsForTaskRepository commentsForTaskRepository)
        {
            _tasksRepository = tasksRepository;
            AspNetUsersRepository = aspNetUsersRepository;
            TeamsRepository = teamsRepository;
            _commentsForTaskRepository = commentsForTaskRepository;
        }

        // GET: CommentsForTasks
        public async Task<ActionResult> Index()
        {
            return RedirectToAction("Index", "Home");
        }

        // GET: CommentsForTasks/Details/5
        public async Task<ActionResult> Details()
        {
            return View();
        }
        [HttpPost]
        public ActionResult AddImage(HttpPostedFileBase fileInput)
        {
            string fileName = fileInput.FileName;
            var image = new Bitmap(fileInput.InputStream, false);
            image.Save(Path.Combine(HttpContext.Server.MapPath("/Content/"), fileName));
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult AddComment(HttpPostedFileBase fileInput, string text, string taskId, string teamId)
        {
            string fileName = fileInput.FileName;
            var image = new Bitmap(fileInput.InputStream, false);
            image.Save(Path.Combine(HttpContext.Server.MapPath("/Content/"), fileName));
            return RedirectToAction("Index");
        }
        // GET: CommentsForTasks/Create
        public async Task<ActionResult> Create()
        {
            ViewBag.TaskId = new SelectList(await _tasksRepository.GetAll().ToListAsync(), "Id", "Name");
            return View();
        }

        // POST: CommentsForTasks/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Text,TaskId,UserId,FileUrl")] CommentsForTask commentsForTask)
        {
            commentsForTask.Id = Guid.NewGuid().ToString();
            commentsForTask.DateCreate=DateTime.Now;
            if (!ModelState.IsValid)
            {
                ViewBag.TaskId = new SelectList(await _tasksRepository.GetAll().ToListAsync(), "Id", "OwnUserId", commentsForTask.TaskId);
                return View(commentsForTask);
            }
            _commentsForTaskRepository.Add(commentsForTask);
            _commentsForTaskRepository.Save();

            return RedirectToAction("Index");
        }

        // GET: CommentsForTasks/Edit/5
        public async Task<ActionResult> Edit(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CommentsForTask commentsForTask = await _commentsForTaskRepository.GetBy(x=>x.Id==id).FirstOrDefaultAsync();
            if (commentsForTask == null)
            {
                return HttpNotFound();
            }
            ViewBag.TaskId = new SelectList(await _tasksRepository.GetAll().ToListAsync(), "Id", "OwnUserId", commentsForTask.TaskId);
            return View(commentsForTask);
        }

        // POST: CommentsForTasks/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Text,TaskId,UserId,FileUrl")] CommentsForTask commentsForTask)
        {
            ViewBag.TaskId = new SelectList(await _tasksRepository.GetAll().ToListAsync(), "Id", "OwnUserId", commentsForTask.TaskId);
            if (!ModelState.IsValid)
            {
                return View(commentsForTask);
            }
            var dbCommentsForTask = await _commentsForTaskRepository.GetBy(x => x.Id == commentsForTask.Id).FirstOrDefaultAsync();
            if(dbCommentsForTask==null)
                return View(commentsForTask);
            dbCommentsForTask.TaskId = commentsForTask.TaskId;
            dbCommentsForTask.Text = commentsForTask.Text;
            dbCommentsForTask.UserId = commentsForTask.UserId;

            _commentsForTaskRepository.Edit(dbCommentsForTask);
            _commentsForTaskRepository.Save();
            return RedirectToAction("Index");
        }

        // GET: CommentsForTasks/Delete/5
        public async Task<ActionResult> Delete(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CommentsForTask commentsForTask = await _commentsForTaskRepository.GetBy(x=>x.Id==id).FirstOrDefaultAsync();
            if (commentsForTask == null)
            {
                return HttpNotFound();
            }
            return View(commentsForTask);
        }

        // POST: CommentsForTasks/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(string id)
        {
            CommentsForTask commentsForTask = await _commentsForTaskRepository.GetBy(x=>x.Id==id).FirstOrDefaultAsync();
            _commentsForTaskRepository.Remove(commentsForTask);
            _commentsForTaskRepository.Save();

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _commentsForTaskRepository.Dispose();
                _tasksRepository.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
