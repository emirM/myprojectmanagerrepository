﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using MyProjectsManager.Data.Repositories;
using MyProjectsManager.Web.Attributes;

namespace MyProjectsManager.Web.Controllers
{
    [LayoutSupport]
    public class HomeController : BaseController
    {
        public override IAspNetUsersRepository AspNetUsersRepository { get; set; }
        public override ITeamsRepository TeamsRepository { get; set; }
        private IProjectsRepository _projectsRepository;
        private IUserInTeamsRepository _userInTeamsRepository;

        public HomeController(IProjectsRepository projectsRepository, IAspNetUsersRepository aspNetUsersRepository, ITeamsRepository teamsRepository,
            IUserInTeamsRepository userInTeamsRepository)
        {
            _projectsRepository = projectsRepository;
            AspNetUsersRepository = aspNetUsersRepository;
            TeamsRepository = teamsRepository;
            _userInTeamsRepository = userInTeamsRepository;
        }

        [Authorize]
        public async Task<ActionResult> Index()
        {
            if (CurrentUser == null) return View();
            var currentUserId = CurrentUser.Id;
            var selectTeamId =
                await _userInTeamsRepository.GetBy(x => x.UserId == currentUserId)
                .Select(x => x.TeamId).ToListAsync();

            var projects = await _projectsRepository
                .GetBy(x => selectTeamId.Contains(x.TeamId)||x.OwnUserId==currentUserId)
                .OrderByDescending(x=>x.DateModified).ToListAsync();
            return View(projects);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult ExampleReturnJson()
        {
            var jsonString = "[{ id: 1, name: 'Spider-man' },{ id: 2, name: 'Betman' },{ id: 3, name: 'Superman' },{ id: 4, name: 'Bombarman' },{ id: 5, name: 'Mario' },{ id: 6, name: 'Dr. X' },{ id: 7, name: 'Rassomaha' },{ id: 8, name: 'Dr. Stranch' },"
                             + "{ id: 9, name: 'Flash' },"
                             + "{ id: 10, name: 'Tor' },"
                             + "{ id: 11, name: 'Capitan America'}"
                             + "]; ";

            return Json(jsonString, JsonRequestBehavior.AllowGet);
        }

    }
}