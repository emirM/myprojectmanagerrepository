﻿using System;
using System.Data.Entity;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using MyProjectsManager.Data;
using MyProjectsManager.Data.Repositories;
using MyProjectsManager.Web.Attributes;

namespace MyProjectsManager.Web.Controllers
{
    [Authorize]
    [LayoutSupport]
    public class TeamsController : BaseController
    {
        public override IAspNetUsersRepository AspNetUsersRepository { get; set; }
        public override ITeamsRepository TeamsRepository { get; set; }

        public TeamsController(IAspNetUsersRepository aspNetUsersRepository, ITeamsRepository teamsRepository)
        {
            AspNetUsersRepository = aspNetUsersRepository;
            TeamsRepository = teamsRepository;
        }
        // GET: Teams
        public async Task<ActionResult> Index()
        {
            return View(await TeamsRepository.GetAll().ToListAsync());
        }

        // GET: Teams/Details/5
        public async Task<ActionResult> Details(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Team team = await TeamsRepository.GetBy(x=>x.Id==id).FirstOrDefaultAsync();
            if (team == null)
            {
                return HttpNotFound();
            }
            return View(team);
        }

        // GET: Teams/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Teams/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Title,DateCreate,DateModified,OwnUserId,ProjectId")] Team team)
        {
            team.Id = Guid.NewGuid().ToString();
            team.DateCreate = DateTime.Now;
            if (!ModelState.IsValid) return View(team);
            TeamsRepository.Add(team);
            TeamsRepository.Save();
            return RedirectToAction("Index");
        }

        // GET: Teams/Edit/5
        public async Task<ActionResult> Edit(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Team team = await TeamsRepository.GetBy(x=>x.Id==id).FirstOrDefaultAsync();
            if (team == null)
            {
                return HttpNotFound();
            }
            return View(team);
        }

        // POST: Teams/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Title,DateCreate,DateModified,OwnUserId,ProjectId")] Team team)
        {
            if (!ModelState.IsValid) return View(team);
            var dbTeam = await TeamsRepository.GetBy(x => x.Id == team.Id).FirstOrDefaultAsync();
            if (dbTeam == null) return View(team);
            dbTeam.DateModified=DateTime.Now;
            UpdateModel(dbTeam);
            TeamsRepository.Save();
            return RedirectToAction("Index");
        }

        // GET: Teams/Delete/5
        public async Task<ActionResult> Delete(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Team team = await TeamsRepository.GetBy(x=>x.Id==id).FirstOrDefaultAsync();
            if (team == null)
            {
                return HttpNotFound();
            }
            return View(team);
        }

        // POST: Teams/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(string id)
        {
            Team team = await TeamsRepository.GetBy(x=>x.Id==id).FirstOrDefaultAsync();
            TeamsRepository.Remove(team);
            TeamsRepository.Save();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                TeamsRepository.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}
