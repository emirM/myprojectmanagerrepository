﻿using System;
using System.Data.Entity;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using MyProjectsManager.Data;
using MyProjectsManager.Data.Repositories;
using MyProjectsManager.Web.Attributes;

namespace MyProjectsManager.Web.Controllers
{
    [Authorize]
    [LayoutSupport]
    public class UserInTeamsController : BaseController
    {
        public override IAspNetUsersRepository AspNetUsersRepository { get; set; }
        public override ITeamsRepository TeamsRepository { get; set; }
        private IUserInTeamsRepository _userInTeamsRepository;

        public UserInTeamsController(IAspNetUsersRepository aspNetUsersRepository, ITeamsRepository teamsRepository,
            IUserInTeamsRepository userInTeamsRepository)
        {
            AspNetUsersRepository = aspNetUsersRepository;
            TeamsRepository = teamsRepository;
            _userInTeamsRepository = userInTeamsRepository;
        }
        // GET: UserInTeams
        public async Task<ActionResult> Index()
        {
            return View(await _userInTeamsRepository.GetBy(x=>x.UserId==CurrentUser.Id).ToListAsync());
        }

        // GET: UserInTeams/Details/5
        public async Task<ActionResult> Details(string id)
        {
            if (String.IsNullOrEmpty(id))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserInTeam userInTeam = await _userInTeamsRepository.GetBy(x=>x.Id==id).FirstOrDefaultAsync();
            if (userInTeam == null)
            {
                return HttpNotFound();
            }
            return View(userInTeam);
        }

        // GET: UserInTeams/Create
        public async Task<ActionResult> Create(string aspnetUserId)
        {
            ViewBag.TeamId = new SelectList(await TeamsRepository.GetBy(x => x.OwnUserId == CurrentUser.Id).ToListAsync(), "Id", "Title");

            //if (String.IsNullOrEmpty(teamId))
            //{
            //    ViewBag.TeamId = new SelectList(await TeamsRepository.GetBy(x => x.OwnUserId == CurrentUser.Id).ToListAsync(), "Id", "Title");

            //}
            //else
            //{
            //    ViewBag.TeamId = new SelectList(await TeamsRepository.GetBy(x => x.OwnUserId == CurrentUser.Id).ToListAsync(), "Id", "Title", teamId);

            //}
            if (String.IsNullOrEmpty(aspnetUserId))
            {
                ViewBag.UserId = new SelectList(await AspNetUsersRepository.GetBy(x => x.Id != CurrentUser.Id).ToListAsync(), "Id", "Email");
            }
            else
            {
                ViewBag.UserId = new SelectList(await AspNetUsersRepository.GetBy(x => x.Id != CurrentUser.Id).ToListAsync(), "Id", "Email", aspnetUserId);
            }
            return View();
        }

        // POST: UserInTeams/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,UserId,TeamId")] UserInTeam userInTeam)
        {
            userInTeam.Id = Guid.NewGuid().ToString();
            if (!ModelState.IsValid)
            {
                ViewBag.UserId = new SelectList(await AspNetUsersRepository.GetBy(x => x.Id != CurrentUser.Id).ToListAsync(), "Id", "Email", userInTeam.UserId);
                ViewBag.TeamId = new SelectList(await TeamsRepository.GetBy(x => x.OwnUserId == CurrentUser.Id).ToListAsync(), "Id", "Title", userInTeam.TeamId);

                return View(userInTeam);
            }
            _userInTeamsRepository.Add(userInTeam);
            _userInTeamsRepository.Save();
            return RedirectToAction("Index");
        }

        // GET: UserInTeams/Edit/5
        public async Task<ActionResult> Edit(string id)
        {
            if (String.IsNullOrEmpty(id))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserInTeam userInTeam = await _userInTeamsRepository.GetBy(x=>x.Id==id).FirstOrDefaultAsync();
            if (userInTeam == null)
            {
                return HttpNotFound();
            }
            ViewBag.UserId = new SelectList(await AspNetUsersRepository.GetBy(x => x.Id != CurrentUser.Id).ToListAsync(), "Id", "Email", userInTeam.UserId);
            ViewBag.TeamId = new SelectList(await TeamsRepository.GetBy(x => x.OwnUserId == CurrentUser.Id).ToListAsync(), "Id", "Title", userInTeam.TeamId);

            return View(userInTeam);
        }

        // POST: UserInTeams/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,UserId,TeamId")] UserInTeam userInTeam)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.UserId = new SelectList(await AspNetUsersRepository.GetBy(x => x.Id != CurrentUser.Id).ToListAsync(), "Id", "Email", userInTeam.UserId);
                ViewBag.TeamId = new SelectList(await TeamsRepository.GetBy(x => x.OwnUserId == CurrentUser.Id).ToListAsync(), "Id", "Title", userInTeam.TeamId);

                return View(userInTeam);
            }
            var dbUserInTeam = await _userInTeamsRepository.GetBy(x => x.Id == userInTeam.Id).FirstOrDefaultAsync();
            dbUserInTeam.UserId = userInTeam.UserId;
            dbUserInTeam.TeamId = userInTeam.TeamId;
            _userInTeamsRepository.Edit(dbUserInTeam);
            _userInTeamsRepository.Save();
            return RedirectToAction("Index");
        }

        // GET: UserInTeams/Delete/5
        public async Task<ActionResult> Delete(string id)
        {
            if (String.IsNullOrEmpty(id))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserInTeam userInTeam = await _userInTeamsRepository.GetBy(x=>x.Id==id).FirstOrDefaultAsync();
            if (userInTeam == null)
            {
                return HttpNotFound();
            }
            return View(userInTeam);
        }

        // POST: UserInTeams/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(string id)
        {
            UserInTeam userInTeam = await _userInTeamsRepository.GetBy(x=>x.Id==id).FirstOrDefaultAsync();
            _userInTeamsRepository.Remove(userInTeam);
            _userInTeamsRepository.Save();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                AspNetUsersRepository.Dispose();
                TeamsRepository.Dispose();
                _userInTeamsRepository.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
