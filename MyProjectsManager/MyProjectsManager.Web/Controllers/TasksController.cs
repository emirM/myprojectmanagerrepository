﻿using System;
using System.Data.Entity;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using MyProjectsManager.Data;
using MyProjectsManager.Data.Repositories;
using MyProjectsManager.Web.Attributes;
using Task = MyProjectsManager.Data.Task;

namespace MyProjectsManager.Web.Controllers
{
    [LayoutSupport]
    [Authorize]
    public class TasksController : BaseController
    {
        public override IAspNetUsersRepository AspNetUsersRepository { get; set; }
        public override ITeamsRepository TeamsRepository { get; set; }
        private IProjectsRepository _projectsRepository;
        private ITasksRepository _tasksRepository;
        private IStatusRepository _statusRepository;

        public TasksController(ITasksRepository tasksRepository, IAspNetUsersRepository aspNetUsersRepository,
            ITeamsRepository teamsRepository, IProjectsRepository projectsRepository, IStatusRepository statusRepository)
        {
            _tasksRepository = tasksRepository;
            AspNetUsersRepository = aspNetUsersRepository;
            TeamsRepository = teamsRepository;
            _projectsRepository = projectsRepository;
            _statusRepository = statusRepository;
        }
        // GET: Tasks
        public async Task<ActionResult> Index()
        {
            return View(await _tasksRepository.GetAll().ToListAsync());
        }

        // GET: Tasks/Details/5
        public async Task<ActionResult> Details(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Task task = await _tasksRepository.GetBy(x=>x.Id==id).FirstOrDefaultAsync();
            if (task == null)
            {
                return HttpNotFound();
            }
            return View(task);
        }

        // GET: Tasks/Create
        public async Task<ActionResult> Create(string projectId)
        {
            ViewBag.ProjectId = new SelectList(await _projectsRepository.GetAll().ToListAsync(), "Id", "Title", projectId);
            ViewBag.ResponsibleUserId = new SelectList(await AspNetUsersRepository.GetAll().ToListAsync(), "Id", "UserName");
            ViewBag.StatusId = new SelectList(await _statusRepository.GetAll().ToListAsync(), "Id", "Name");
            return View();
        }

        // POST: Tasks/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,OwnUserId,ProjectId,Title,Description,DescriptionWithoutHtml,Prioritet,ResponsibleUserId,DateCreate,DateModified,DatePlanBeginnig,DateFactBeginning,DateComplete,StatusId,Deadline,DateFactEnding")] Task task)
        {
            task.Id = Guid.NewGuid().ToString();
            task.DateCreate=DateTime.Now;
             //var datePlanning= new DateTime(task.DatePlanBeginnig.Year, task.DatePlanBeginnig.Month, task.DatePlanBeginnig.Day, task.DatePlanBeginnig.Hour, task.DatePlanBeginnig.Minute, task.DatePlanBeginnig.Second);
            task.DatePlanBeginnig = new DateTime(task.DatePlanBeginnig.Year, task.DatePlanBeginnig.Month, task.DatePlanBeginnig.Day, task.DatePlanBeginnig.Hour, task.DatePlanBeginnig.Minute, task.DatePlanBeginnig.Second);
            //task.OwnUserId = CurrentUser.Id;
            if (!ModelState.IsValid)
            {
                ViewBag.ProjectId = new SelectList(await _projectsRepository.GetAll().ToListAsync(), "Id", "Title", task.ProjectId);
                ViewBag.StatusId = new SelectList(await _statusRepository.GetAll().ToListAsync(), "Id", "Name", task.StatusId);
                ViewBag.ResponsibleUserId = new SelectList(await AspNetUsersRepository.GetAll().ToListAsync(), "Id", "UserName", task.ResponsibleUserId);
                return View(task);
            }
            _tasksRepository.Add(task);
            _tasksRepository.Save();
            var project = await _projectsRepository.GetBy(x => x.Id == task.ProjectId).FirstOrDefaultAsync();
            if (project != null)
            {
                project.DateModified=DateTime.Now;
                _projectsRepository.Edit(project);
                _projectsRepository.Save();
            }
            return RedirectToAction("Index","Home");
        }

        // GET: Tasks/Edit/5
        public async Task<ActionResult> Edit(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Task task = await _tasksRepository.GetBy(x=>x.Id==id).FirstOrDefaultAsync();
            if (task == null)
            {
                return HttpNotFound();
            }
            ViewBag.ProjectId = new SelectList(await _projectsRepository.GetAll().ToListAsync(), "Id", "Title", task.ProjectId);
            //ViewBag.StatusId = new SelectList(await _statusRepository.GetAll().ToListAsync(), "Id", "Name", task.StatusId);
            ViewBag.ResponsibleUserId = new SelectList(await AspNetUsersRepository.GetAll().ToListAsync(), "Id", "UserName", task.ResponsibleUserId);
            var project = await _projectsRepository.GetBy(x => x.Id == task.ProjectId).FirstOrDefaultAsync();
            if (project != null)
            {
                project.DateModified = DateTime.Now;
                _projectsRepository.Edit(project);
                _projectsRepository.Save();
            }
            return View(task);
        }

        // POST: Tasks/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,OwnUserId,ProjectId,Title,Description,DescriptionWithoutHtml,Prioritet,ResponsibleUserId,DateCreate,DateModified,DatePlanBeginnig,DateFactBeginning,DateComplete,StatusId,Deadline,DateFactEnding")] Task task)
        {
            ViewBag.ProjectId = new SelectList(await _projectsRepository.GetAll().ToListAsync(), "Id", "Title", task.ProjectId);
            ViewBag.StatusId = new SelectList(await _statusRepository.GetAll().ToListAsync(), "Id", "Name", task.StatusId);
            ViewBag.ResponsibleUserId = new SelectList(await AspNetUsersRepository.GetAll().ToListAsync(), "Id", "UserName", task.ResponsibleUserId);

            if (!ModelState.IsValid)
            {
                return View(task);
            }
            var dbTask = await _tasksRepository.GetBy(x => x.Id == task.Id).FirstOrDefaultAsync();
            if (dbTask == null)
            {
                return View(task);
            }
            UpdateModel(dbTask);
            _tasksRepository.Save();
            var project = await _projectsRepository.GetBy(x => x.Id == task.ProjectId).FirstOrDefaultAsync();
            if (project != null)
            {
                project.DateModified = DateTime.Now;
                _projectsRepository.Edit(project);
                _projectsRepository.Save();
            }
            return RedirectToAction("Index", "Home");
        }

        // GET: Tasks/Delete/5
        public async Task<ActionResult> Delete(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Task task = await _tasksRepository.GetBy(x=>x.Id==id).FirstOrDefaultAsync();
            if (task == null)
            {
                return HttpNotFound();
            }
            var project = await _projectsRepository.GetBy(x => x.Id == task.ProjectId).FirstOrDefaultAsync();
            if (project != null)
            {
                project.DateModified = DateTime.Now;
                _projectsRepository.Edit(project);
                _projectsRepository.Save();
            }
            return View(task);
        }

        public async Task<ActionResult> SpentTimeSet(string id, string time)
        {
            var result = new Error() { Code = 0, Description = "OK", Result = "" };
            if (String.IsNullOrEmpty(id) || string.IsNullOrEmpty(time))
            {
                result.Code = 1;
                result.Description = "id or time is null";
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            var task = await _tasksRepository.GetBy(x => x.Id == id).FirstOrDefaultAsync();
            if (task==null)
            {
                result.Code = 2;
                result.Description = "not found works";
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            task.SpentTime = time;
            _tasksRepository.Edit(task);
            _tasksRepository.Save();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> Complete(string id, bool? isComplete)
        {
            var result = new Error() { Code = 0, Description = "OK", Result = "" };
            if (String.IsNullOrEmpty(id) || isComplete==null)
            {
                result.Code = 1;
                result.Description = "id or isComplete is null";
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            var task = await _tasksRepository.GetBy(x => x.Id == id).FirstOrDefaultAsync();
            if (task == null)
            {
                result.Code = 2;
                result.Description = "not found works";
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            task.DateComplete = isComplete==true?DateTime.Now : (DateTime?) null;
            _tasksRepository.Edit(task);
            _tasksRepository.Save();
            var project = await _projectsRepository.GetBy(x => x.Id == task.ProjectId).FirstOrDefaultAsync();
            if (project != null)
            {
                project.DateModified = DateTime.Now;
                _projectsRepository.Edit(project);
                _projectsRepository.Save();
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        // POST: Tasks/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(string id)
        {
            Task task = await _tasksRepository.GetBy(x => x.Id == id).FirstOrDefaultAsync();
            _tasksRepository.Remove(task);
            _tasksRepository.Save();
            var project = await _projectsRepository.GetBy(x => x.Id == task.ProjectId).FirstOrDefaultAsync();
            if (project != null)
            {
                project.DateModified = DateTime.Now;
                _projectsRepository.Edit(project);
                _projectsRepository.Save();
            }
            return RedirectToAction("Index","Home");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _tasksRepository.Dispose();
                _projectsRepository.Dispose();
                AspNetUsersRepository.Dispose();
                TeamsRepository.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
