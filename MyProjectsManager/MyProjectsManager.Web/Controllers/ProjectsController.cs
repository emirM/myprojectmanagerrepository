﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using MyProjectsManager.Data;
using MyProjectsManager.Data.Repositories;
using MyProjectsManager.Web.Attributes;

namespace MyProjectsManager.Web.Controllers
{
    [LayoutSupport]
    public class ProjectsController : BaseController
    {
        private IProjectsRepository _projectsRepository;
        public override IAspNetUsersRepository AspNetUsersRepository { get; set; }
        public override ITeamsRepository TeamsRepository { get; set; }
        private IUserInTeamsRepository _userInTeamsRepository;

        public ProjectsController(IProjectsRepository projectsRepository, IAspNetUsersRepository aspNetUsersRepository,
            ITeamsRepository teamsRepository, IUserInTeamsRepository userInTeamsRepository)
        {
            _projectsRepository = projectsRepository;
            AspNetUsersRepository = aspNetUsersRepository;
            TeamsRepository = teamsRepository;
            _userInTeamsRepository = userInTeamsRepository;
        }

        // GET: Projects
        public async Task<ActionResult> Index()
        {
            var currentUserId = CurrentUser.Id;
            var selectTeamId =
                await _userInTeamsRepository.GetBy(x => x.UserId == currentUserId)
                .Select(x => x.TeamId).ToListAsync();

            var projects = await _projectsRepository
                .GetBy(x => selectTeamId.Contains(x.TeamId) || x.OwnUserId == currentUserId)
                .OrderByDescending(x => x.DateModified).ToListAsync();

            return View(projects);
        }

        // GET: Projects/Details/5
        public async Task<ActionResult> Details(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Project project = await _projectsRepository.GetBy(x=>x.Id==id).FirstOrDefaultAsync();
            if (project == null)
            {
                return HttpNotFound();
            }
            return View(project);
        }

        // GET: Projects/Create
        public async Task<ActionResult> Create()
        {
            ViewBag.TeamId = new SelectList(await TeamsRepository.GetAll().ToListAsync(), "Id", "Title");
            return View();
        }

        // POST: Projects/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Title,DateCreate,OwnUserId,TeamId,DateModified")] Project project)
        {
            project.Id = Guid.NewGuid().ToString();
            project.DateCreate=DateTime.Now;
            if (!ModelState.IsValid)
            {
                ViewBag.TeamId = new SelectList(await TeamsRepository.GetAll().ToListAsync(), "Id", "Title", project.TeamId);
                return View(project);
            }

            _projectsRepository.Add(project);
            _projectsRepository.Save();
            return RedirectToAction("Index","Home");
        }

        // GET: Projects/Edit/5
        public async Task<ActionResult> Edit(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Project project = await _projectsRepository.GetBy(x=>x.Id==id).FirstOrDefaultAsync();
            if (project == null)
            {
                return HttpNotFound();
            }
            ViewBag.TeamId = new SelectList(await TeamsRepository.GetAll().ToListAsync(), "Id", "Title", project.TeamId);
            return View(project);
        }

        // POST: Projects/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Title,DateCreate,OwnUserId,TeamId,DateModified")] Project project)
        {
            ViewBag.TeamId = new SelectList(await TeamsRepository.GetAll().ToListAsync(), "Id", "Title", project.TeamId);
            if (!ModelState.IsValid)
            {
                return View(project);
            }
            var dbProject = await _projectsRepository.GetBy(x => x.Id == project.Id).FirstOrDefaultAsync();
            if (dbProject == null) return View(project);
            dbProject.DateModified = DateTime.Now;
            dbProject.Title = project.Title;
            dbProject.TeamId = project.TeamId;
            _projectsRepository.Edit(dbProject);
            _projectsRepository.Save();

            return RedirectToAction("Index", "Home");
        }

        // GET: Projects/Delete/5
        public async Task<ActionResult> Delete(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Project project = await _projectsRepository.GetBy(x=>x.Id==id).FirstOrDefaultAsync();
            if (project == null)
            {
                return HttpNotFound();
            }
            return View(project);
        }

        // POST: Projects/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return View();
            }
            Project project = await _projectsRepository.GetBy(x => x.Id == id).FirstOrDefaultAsync();
            if (project == null) return View();
            _projectsRepository.Remove(project);
            _projectsRepository.Save();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _projectsRepository.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
