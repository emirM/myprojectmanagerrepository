﻿using System.Data.Entity;
using System.Threading.Tasks;
using System.Web.Mvc;
using MyProjectsManager.Data.Repositories;
using MyProjectsManager.Web.Attributes;

namespace MyProjectsManager.Web.Controllers
{
    [Authorize]
    [LayoutSupport]
    public class AspNetUsersController : BaseController
    {
        public override IAspNetUsersRepository AspNetUsersRepository { get; set; }
        public override ITeamsRepository TeamsRepository { get; set; }

        public AspNetUsersController(IAspNetUsersRepository aspNetUsersRepository, ITeamsRepository teamsRepository)
        {
            AspNetUsersRepository = aspNetUsersRepository;
            TeamsRepository = teamsRepository;
        }
        // GET: AspNetUsers
        public async Task<ActionResult> Index()
        {
            return View(await AspNetUsersRepository.GetBy(x=>x.Id!=CurrentUser.Id).ToListAsync());
        }
        

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                AspNetUsersRepository.Dispose();
                TeamsRepository.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
