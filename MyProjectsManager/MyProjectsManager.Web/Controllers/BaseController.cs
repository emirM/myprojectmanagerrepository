﻿using System.Web.Mvc;
using MyProjectsManager.Data;
using MyProjectsManager.Data.Repositories;

namespace MyProjectsManager.Web.Controllers
{
    public abstract class BaseController : Controller
    {
    //    public Language Language { get; set; }
    //    public abstract ILanguagesRepository LanguagesRepository { get; set; }
        public abstract IAspNetUsersRepository AspNetUsersRepository { get; set; }
        public abstract ITeamsRepository TeamsRepository { get; set; }
        public AspNetUser CurrentUser { get; set; }

    }
}