﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MyProjectsManager.Web.Startup))]
namespace MyProjectsManager.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
