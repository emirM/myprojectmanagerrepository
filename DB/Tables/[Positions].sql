Create table [Positions](
 [Id] nvarchar(128) Primary key,
 [Title] nvarchar(256) not null,
 [DateCreate] datetime not null,
 [DateModified] datetime
)