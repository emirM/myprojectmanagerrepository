Create table [CommentsForTasks](
[Id] nvarchar(128) Primary key,
[Text] text not null,
[TaskId] nvarchar(128),
[UserId] nvarchar(128),
[FileUrl] nvarchar(2083)
)