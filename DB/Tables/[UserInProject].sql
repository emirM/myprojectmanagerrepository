Create table [UserInProject](
 [Id] nvarchar(128) Primary key,
 [UserId] nvarchar(128) not null,
 [ProjectId] nvarchar(128) not null,
 [DateCreate] datetime
)