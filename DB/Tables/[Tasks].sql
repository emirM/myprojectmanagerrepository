Create table [Tasks](
 [Id] nvarchar(128) Primary key,
 [OwnUserId] nvarchar(128) not null,
 [ProjectId] nvarchar(128),

--Description
 [Title] nvarchar(256) not null,
 [Description] text,
 [DescriptionWithoutHtml] text,
 [Prioritet] int, --min 0, max 100
 [ResponsibleUserId] nvarchar(128) not null,

--Date
 [DateCreate] datetime not null,
 [DateModified] datetime,
 [DatePlanBeginnig] datetime not null, --���� ��������� ������
 [DateFactBeginning] datetime, --���� ������������ ������
 [DateComplete] datetime, --���� ����������
 
 
)