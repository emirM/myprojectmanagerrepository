Create table [Projects](
 [Id] nvarchar(128) Primary key,
 [Title] nvarchar(256) not null,
 [DateCreate] datetime not null,
 [OwnUserId] nvarchar(128) not null,
 [TeamId] nvarchar(128),
 [DateModified] datetime
)