Create Table [Teams](
 [Id] nvarchar(128) Primary key,
 [Title] nvarchar(250) not null,
 [DateCreate] datetime not null,
 [DateModified] datetime,
 [OwnUserId] nvarchar(128) not null,
 [ProjectId] nvarchar(128)
)